# Override Rails ActiveStorage because RAW_POST_DATA is slightly different when uploading to Alwaysdata.
# This difference prevents JS checksum from matching the one calculated by Ruby from the uploaded file.
# See https://forum.alwaysdata.com/viewtopic.php?pid=21242#p21242
class UploadController < ActiveStorage::DiskController
  def update
    # Remove comment to fix file upload error caused by uWSGI
    # request.env['RAW_POST_DATA'] = request.body.read(request.content_length)
    super
  end
end
