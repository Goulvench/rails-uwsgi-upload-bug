Rails.application.routes.draw do
  resources :articles

  # Override Rails ActiveStorage because RAW_POST_DATA is slightly different when uploading to Alwaysdata.
  # This difference prevents JS checksum from matching the one calculated by Ruby from the uploaded file.
  # See https://forum.alwaysdata.com/viewtopic.php?pid=21242#p21242
  put '/rails/active_storage/disk/:encoded_token', to: 'upload#update'
end
