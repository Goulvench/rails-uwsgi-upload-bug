# README

Configurer Ruby, rbenv est la méthode la plus courante.

Installer [nodejs](https://nodejs.org/en/), [yarn](https://yarnpkg.com/), [rbenv](https://github.com/rbenv/rbenv), une version récente de Ruby (la plus récente sur Alwaysdata est la 2.6.2), et [bundler](https://bundler.io/) si nécessaire (il est en cours d'inclusion dans Ruby).

Dans le dossier de l'application, lancer les commandes suivantes :

```
bundle install
bundle exec rails db:setup
bundle exec rails db:migrate
bundle exec rails s
```

[Ouvrir l'application](http://localhost:3000/articles/new) et déposer une image sur l'éditeur Trix.
Le checksum est calculé par Spark-md5 et envoyé dans la première requête. Le log de l'application affichera une 422 si le checksum ne correspond pas.

